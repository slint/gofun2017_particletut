# GOFUN 2017 - Particle Simulation with OpenFOAM (slides, solver and tutorial case (particle-laden backward-facing step flow)) #

### License ###
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Copyright Information ###
    
    Copyright (C) 1991-2009 OpenCFD Ltd.
    Copyright (C) 2014-2017 Robert Kasper

### Author ###
    
    Robert Kasper <robert.kasper@uni-rostock.de>
    Website: www.lemos.uni-rostock.de

### Notes ###
    
    Compatibility: OpenFOAM-4.x

### Contents ###
    * slides: GOFUN 2017 tutorial presentation
    * solver: pimpleLPTFoam (pimpleFoam including Lagrangian-Particle-Tracking (LPT) with four-way coupling)
    * case: Tutorial case (particle-laden backward-facing step flow)
    * guides: OpenFOAM user/programmer's guide

### Disclaimer ###

    This offering is not approved or endorsed by OpenCFD Limited, the producer
    of the OpenFOAM software and owner of the OPENFOAM®  and OpenCFD®  trade marks.

    Detailed information on the OpenFOAM trademark can be found at

     - http://www.openfoam.com/legal/trademark-policy.php
     - http://www.openfoam.com/legal/trademark-guidelines.php

    For further information on OpenCFD and OpenFOAM, please refer to

     - http://www.openfoam.com